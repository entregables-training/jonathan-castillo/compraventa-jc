package com.sofka.compraventaautomoviles.web.controller;

import com.sofka.compraventaautomoviles.persistence.entity.Vehicle;
import com.sofka.compraventaautomoviles.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;

import java.util.List;

import java.util.Optional;

@RestController
@RequestMapping("automovil")
public class CompraventaController {

    @Autowired
    VehicleService service;

    @GetMapping("all")
    public ResponseEntity<List<Vehicle>> getAll(){
        return new ResponseEntity<>(service.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{placaId}")
    public ResponseEntity<Vehicle> getById(@PathVariable("placaId") String placaId) {
        Optional<Vehicle> vehicle = service.getById(placaId);
        return vehicle.map(value -> new ResponseEntity<>(value, HttpStatus.OK)).orElseGet(()
                -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("save")
    public ResponseEntity<Vehicle> save(@RequestBody Vehicle vehicle){
        return new ResponseEntity<>(service.save(vehicle), HttpStatus.CREATED);
    }
    @PutMapping("update")
    public ResponseEntity<Vehicle> update(@RequestBody Vehicle vehicle) {
        return new ResponseEntity<>(service.update(vehicle), HttpStatus.OK);
    }
    @DeleteMapping("delete/{placaId}")
    public ResponseEntity delete(@PathVariable("placaId") String placaId) {
        if(service.delete(placaId)){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
