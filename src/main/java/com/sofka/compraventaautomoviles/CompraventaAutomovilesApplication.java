package com.sofka.compraventaautomoviles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompraventaAutomovilesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompraventaAutomovilesApplication.class, args);
	}

}
