package com.sofka.compraventaautomoviles.service;

import com.sofka.compraventaautomoviles.persistence.entity.Vehicle;
import com.sofka.compraventaautomoviles.persistence.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VehicleService {

    @Autowired
    private VehicleRepository repository;

    public List<Vehicle> getAll(){
        return repository.findAll();
    }

    public Vehicle save(Vehicle vehicle){
        return repository.save(vehicle);
    }

    public Vehicle update(Vehicle vehicle){
        return repository.save(vehicle);
    }

    public boolean delete(String placaId) {
        return getById(placaId).map(student -> {
            repository.deleteById(placaId);
            return true;
        }).orElse(false);
    }
    public Optional<Vehicle> getById(String placaId) {
        return repository.findById(placaId);
    }
}
