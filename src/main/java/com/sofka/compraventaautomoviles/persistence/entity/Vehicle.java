package com.sofka.compraventaautomoviles.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "automovil")
public class Vehicle {

    @Id
    private String placaId;

    @Column(nullable = false)
    private String marca;

    @Column(nullable = false)
    private String modelo;

    @Column(nullable = false)
    private double kilometraje;

    @Column(nullable = false)
    private int precio;

    @Column(nullable = false)
    private String dueno;

    @Column(nullable = false)
    private long telefonoDueno;

    public String getPlacaId() {
        return placaId;
    }

    public void setPlacaId(String placaId) {
        this.placaId = placaId;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public double getKilometraje() {
        return kilometraje;
    }

    public void setKilometraje(double kilometraje) {
        this.kilometraje = kilometraje;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public String getDueno() {
        return dueno;
    }

    public void setDueno(String dueno) {
        this.dueno = dueno;
    }

    public long getTelefonoDueno() {
        return telefonoDueno;
    }

    public void setTelefonoDueno(long telefonoDueno) {
        this.telefonoDueno = telefonoDueno;
    }
}
