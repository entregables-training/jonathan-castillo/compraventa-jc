package com.sofka.compraventaautomoviles.persistence.repository;

import com.sofka.compraventaautomoviles.persistence.entity.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface  VehicleRepository extends JpaRepository<Vehicle, String> {
}
